import numpy as np
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import InputLayer
from keras.layers import Activation

def build_model():
	# Neural Net for Deep-Q learning Model
	state = [[1,2,3,4,5,6,7,8,9,10, 0, 0, 0, 0, 0, 0, 1, 4, 5, 6]]
	a = np.array(state)	
	model = Sequential()
	model.add(Dense(21, input_dim=20, activation='linear'))
	model.add(Dense(150, activation='sigmoid'))
	model.add(Dense(4, activation='linear'))
	model.compile(loss='mse', optimizer='adam', metrics=['mse', 'mae'])
	target_f = model.predict(a)
	max_q = np.argmax(target_f)
	print(target_f)
	# print(max_q)
	target_f[0][max_q] = -0.5
	model.fit(a, target_f.reshape(-1, 4), epochs=50, verbose=0)
	result = model.predict(a)
	print(result)

build_model()