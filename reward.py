import constants

def money_for_state(state):
	agentOneCash = state[3][0]
	agentTwoCash = state[3][1]
	agentOnePropertyWorth = 0
	agentTwoPropertyWorth = 0
	
	for i in constants.property_to_space_map:
		#In 0 to 39 board position range
		propertyValue =  state[1][i]
		propertyPosition = constants.board[ constants.property_to_space_map[ i ] ]
		
		if propertyValue in range(-6,0):
			agentTwoPropertyWorth += (propertyPosition['price'] + ( (abs(propertyValue)-1)*propertyPosition['build_cost'] ) )
		elif propertyValue == -7:
			agentTwoPropertyWorth += (propertyPosition['price']/2)
		elif propertyValue in range(1,7):
			agentOnePropertyWorth += (propertyPosition['price'] + ( (propertyValue-1)*propertyPosition['build_cost'] ) )
		elif propertyValue == 7:
			agentOnePropertyWorth += (propertyPosition['price']/2)
	
	if state[1][28] == -1:
		agentTwoPropertyWorth += 50
	elif state[1][28] == 1:
		agentOnePropertyWorth += 50
	
	if state[1][29] == -1:
		agentTwoPropertyWorth += 50
	elif state[1][29] == 1:
		agentOnePropertyWorth += 50
		
	print("agent one property worth is :",agentOnePropertyWorth)
	print("agent two property worth is :",agentTwoPropertyWorth)
	print("agent one cash is :",agentOneCash)
	print("agent two cash is :",agentTwoCash)
	
	return ((agentOnePropertyWorth,agentTwoPropertyWorth,agentOneCash,agentTwoCash))
		
		
# money=money_for_state([4, [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, -1, 1, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [19, 19], [1250, 1160], 5, [], [2, 16, 0, 0]])
money = money_for_state((95, (0, -1, 0, -1, 0, 1, -1, 0, 1, -1, 0, 1, -1, -1, 0, -1, 1, 0, 1, 1, 0, 1, 0, 1, -1, -1, 1, -1, 1, 0, 0, -1, 1, 0, 1, 1, 0, 1, 0, -1, 0, 1), (-1, 14), (781, 1899), 3, 14, (0, 0, 0, 160)))

print(money)



def reward_for_agent1(money):
	v=money[0]-money[1]
	p=2
	m=money[2]/(money[2]+money[3])
	r=((v/p)/(1+abs(v/p)))+(m/p)
	print("reward for agent 1 is ",r)
	
def reward_for_agent2(money):
	v=money[1]-money[0]
	p=2
	m=money[3]/(money[2]+money[3])
	r=((v/p)/(1+abs(v/p)))+(m/p)
	print("reward for agent 2 is ",r)
	
reward_for_agent1(money)
reward_for_agent2(money)

