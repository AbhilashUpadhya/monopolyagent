from adjudicator import Adjudicator
from agent import Agent
from RandomAgent import RandomAgent
# from GreedyBuyAgent import GreedyBuyAgent
# from GreedyBuyAndMortgageAgent import GreedyBuyAndMortgageAgent
# from GreedyBuyAndBuildAgent import GreedyBuyAndBuildAgent
from collections import Counter

buyPropertyModel1 = None
bsmModel1 = None
winnerCounter = Counter()
for i in range(100):
	agentOne= Agent(1, buyPropertyModel1,bsmModel1)
	agentTwo = RandomAgent(2)
	# agentTwo = GreedyBuyAgent(2)
	# agentTwo = GreedyBuyAndMortgageAgent(2)
	# agentTwo = GreedyBuyAndBuildAgent(2)
	adjudicator = Adjudicator()
	winner, finalState = adjudicator.runGame(agentOne, agentTwo)
	buyPropertyModel1 = agentOne.getBuyPropertyModel()
	bsmModel1 = agentOne.getBsmModel()
	
	if i%100 == 0:
		print(i)
	winnerCounter[winner] += 1
print(winnerCounter)